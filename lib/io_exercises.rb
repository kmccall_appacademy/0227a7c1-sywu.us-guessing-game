# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

# class GuessingGame
#   attr_reader :random_num
#   def initialize
#     @random_num = rand(1..100)
#   end
#
#   def play
#     puts "Enter a guess"
#     guess = gets.chomp.to_i
#     count = 1
#     while guess != random_num
#       if guess > random_num
#         puts "Too high"
#       else
#         puts "Too low"
#       end
#       puts "Enter a new guess"
#       guess = gets.chomp.to_i
#       count += 1
#     end
#
#     puts "Congratulations You Won"
#     puts "You Won in #{count} guesses"
#   end
# end
# game = GuessingGame.new
# game.play

def guessing_game
    random_num = rand(1..100)
    puts "guess a number"
    guess = gets.chomp.to_i
    count = 1
    while guess != random_num
      puts guess
      if guess > random_num
        puts "too high"
      else
        puts "too low"
      end
      puts "enter a new guess"
      guess = gets.chomp.to_i
      count += 1
    end
    puts guess
    puts count
    # puts "Congratulations You Won"
    # puts "You Won in #{count} guesses"
end
